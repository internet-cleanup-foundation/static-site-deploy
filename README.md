# static-site-deploy

Declarative way to deploy all static sites from ICF. Supports gitlab pages, hugo and redirects. Let's not configure this by hand anymore.

WaRnInG: Has not yet been tested with a blank / clean nginx or webserver.


# System requirements

Make sure the webserver has access to python3, git, hugo and nginx.


# Example

To deploy all sites in sites.txt, run the following commands.

```
git clone https://gitlab.com/internet-cleanup-foundation/static-site-deploy/
sudo python3 deploy.py
```

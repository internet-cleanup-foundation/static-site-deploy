#!/usr/bin/python3
import json
import subprocess
import os
import shutil
import time

nginx_config_path = "/etc/nginx/sites-enabled/"
webroots = "/var/www/"

# IMPORTANT:
# You must have the following tools installed:
# git, hugo, nginx,
# tested with python3

# Make sure the DNS is setup correctly, otherwise you'll get an error when creating a TLS cert
# From sites the "public" map is published, the rest is ignored.


def setup_site_in_nginx(address, redirect_url=None):
    def create_webroot(path):
        # you always need to make files on the system as letsencrypt needs them
        if not os.path.isdir(path):
            os.mkdir(path)

    # you can't start nginx if your cert files are emissing. But you want nginx to run to get certificates
    # this chicken and egg problem is solved to first run only on http, get the cert and then do full https.
    def create_config(params, http_only=False):

        http_config = """
server {
        listen 80;
        listen [::]:80;
        server_name %(address)s;
        root %(webroots)s%(address)s;

        # support lets encrypt certificate generation   
        location /.well-known { }

        %(redirect_to_other_site)s
        %(redirect_to_https)s
}
""" % params

        https_config = """
server {
        root %(webroots)s%(address)s;
        server_name %(address)s;
        
        index index.html;

        include global/restrictions.conf;
        listen 443 ssl;
        listen [::]:443 ssl;
        ssl_certificate /etc/letsencrypt/live/%(address)s/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/%(address)s/privkey.pem;
        ssl_dhparam /etc/nginx/certificates/dhparam.pem;

        ssl_session_cache shared:SSL:20m;
        ssl_session_timeout 10m;
        ssl_protocols TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers 'ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5';
        ssl_stapling on;
        ssl_stapling_verify on;

        %(redirect_to_other_site)s
} """ % params

        if http_only:
            return http_config

        return http_config + https_config

    def write_config(location, config):
        with open(location, 'w') as out:
            out.write(config)

    def restart_ngninx():
        # nginx.service start request repeated too quickly, refusing to start.
        # don't be too succesful, just wait a few seconds :)
        # normal limit is 5 restarts in 10 seconds.
        time.sleep(2.1)

        # makes the new paths / site available to the world
        command = "nginx -t"
        subprocess.check_call(command.split(" "))

        command = "service nginx restart"
        subprocess.check_call(command.split(" "))

    def request_certificate(params):
        cert = "certbot certonly --keep --webroot -w %(webroots)s%(address)s/ -d %(address)s" % params
        subprocess.check_call(cert.split(" "))

    redirect_to_other_site = ""
    redirect_to_https = ""
    if redirect_url:
        redirect_to_other_site = """
            location / {
                return 301 %s;
            }
            """ % redirect_url
    else:
        redirect_to_https = "location / {return 301 https://$host$request_uri;}"

    parameters = {'address': address, 'redirect_to_other_site': redirect_to_other_site,
                  'redirect_to_https': redirect_to_https, 'webroots': webroots}

    create_webroot("/var/www/%(address)s/" % parameters)
    write_config(nginx_config_path + address, create_config(parameters, http_only=True))
    restart_ngninx()
    request_certificate(parameters)
    write_config(nginx_config_path + address, create_config(parameters))
    restart_ngninx()


def deploy_content_from_gitlab(address, site):
    site_path = webroots + address

    # clear existing content
    shutil.rmtree(site_path)

    os.mkdir(site_path)

    # deploy the new content
    shellcommand("/usr/bin/git clone --recursive %s temp" % site['repository'], cwd=site_path)

    # move the output to the webroot:
    content = os.listdir("%s/temp/public/" % site_path)
    for item in content:
        shutil.move("%s/temp/public/%s" % (site_path, item), site_path + "/")

    # clean up temp files
    shutil.rmtree("%(site_path)s/temp/" % {'site_path': site_path})


def deploy_content_from_hugo(address, site):
    site_path = webroots + address

    # clear existing content
    shutil.rmtree(site_path)
    os.mkdir(site_path)

    # deploy the new content
    shellcommand("/usr/bin/git clone --recursive %s temp" % site['repository'], cwd=site_path)
    shellcommand("/usr/local/bin/hugo", cwd="%(site_path)s/temp" % {'site_path': site_path})

    # move the output to the webroot:
    # move results in stat errors
    content = os.listdir("%s/temp/public/" % site_path)
    print(content)
    for item in content:
        shutil.move("%s/temp/public/%s" % (site_path, item), site_path + "/")

    # remove the tmp dir.
    shutil.rmtree("%(site_path)s/temp/" % {'site_path': site_path})


def shellcommand(command, cwd=None):
    print("Running %s in %s." % (command.split(" "), cwd))

    if cwd:
        subprocess.check_call(command.split(" "), cwd=cwd)
    else:
        subprocess.check_call(command.split(" "))


def create_redirects(address, site):
    for redirect in site['redirects-to-this-site']:
        setup_site_in_nginx(address=redirect, redirect_url="https://" + address)


def apply(static_websites):
    for address in sorted(static_websites.keys()):
        print("Deploying %s" % address)

        site = sites[address]

        if site['type'] == "redirect":
            setup_site_in_nginx(address=address, redirect_url=site['target'])

        if site['type'] == "static-site-from-gitlab":
            setup_site_in_nginx(address=address)
            deploy_content_from_gitlab(address, site)

        if site['type'] == "hugo-static-site-from-gitlab":
            setup_site_in_nginx(address=address)
            deploy_content_from_hugo(address, site)

        if "redirects-to-this-site" in site:
            create_redirects(address, site)


with open('sites.json') as f:
    sites = json.load(f)

apply(sites)
